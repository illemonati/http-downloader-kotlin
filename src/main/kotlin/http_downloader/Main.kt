package http_downloader

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking


class MainCommandParser : CliktCommand() {
    private val url : String by option(help="URL to download").required()
    private val filename : String? by option(help="filename to save as")
    override fun run() {
        runBlocking {
            download(url, filename)
        }
    }
}


fun main(args: Array<String>) {
    MainCommandParser().main(args)
}
