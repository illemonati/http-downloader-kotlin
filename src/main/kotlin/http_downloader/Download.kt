package http_downloader

import io.ktor.client.HttpClient
import io.ktor.client.call.receive
import io.ktor.client.request.get
import io.ktor.client.statement.HttpResponse
import java.io.File
import java.io.InputStream

suspend fun download(url: String, _fileName: String?) {
    val client = HttpClient()
    var fileName = _fileName
    val response = client.get<HttpResponse>(url)
    val fileNameRegex = Regex("(?<=filename=).*?(?=[;|\n])")
    fileName ?: run {
        if (response.headers.contains("Content-Disposition")) {
            fileName = fileNameRegex.find((response.headers["Content-Disposition"] ?: "") + "\n")?.value
        }
    }
    fileName = fileName ?: url
    File(fileName!!).outputStream().use {
        response.receive<InputStream>().copyTo(it)
    }
}